package wth;
//import stuff
//This is what allows windows to be created
import javax.swing.JFrame;
//tells the program where to draw? overrides the paint method 
import javax.swing.JPanel;
//allows drawing shapes etc.
import java.awt.Graphics;
import java.awt.Graphics2D;
//allows colour
import java.awt.Color;
import java.awt.RenderingHints;

@SuppressWarnings("serial")
public class Game extends JPanel{
	int x = 0;
	static int y = 254;
	int z = 232;
	int a = 0;
	int b = 254;
	int c = 232;
	int LMI = 1;
	
	public void m(){
		//changes x and y values of diagonal moving squares
	x += LMI;
	y -= LMI;
	z -= LMI;
	}
	public void n(){
		//changes x and y values of other squares
		a += LMI;
		b -= LMI;
		c -= LMI;}

	@Override
	public void paint(Graphics og){
		super.paint(og);
	Graphics2D g = (Graphics2D) og;
	g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		RenderingHints.VALUE_ANTIALIAS_ON);
	//sets colour to those RGB values
	g.setColor(new Color(200,100,200));
	g.fillRect(0, 0, 300, 300);
	g.setColor(new Color(255,124,65));
	g.fillRect(y-175, x, x, x);
	g.fillRect(125, a-30, 30, 130);
	g.fillRect(b-80, 115, 126, 30);
	g.setColor(new Color(200,79,155));
	g.fillRect(x, x, x, x);
	g.fillRect(y-49, z-49, y+30, y+30);
	g.fillRect(125, c-80, 30, 138);
	g.fillRect(a-20, 115, 130, 30);
	g.setColor(new Color(255,124,65));
	g.fillRect(x, y-71, y+30, y+30);
	g.fillRect(x, x, 30, 30);
	g.fillRect(y, z, 30, 30);
	g.fillRect(a, 115, 30, 30);
	g.fillRect(125, c, 30, 30);
	g.setColor(new Color(200,79,155));
	g.fillRect(y, x, 30, 30);
	g.fillRect(x, z, 30, 30);
	g.fillRect(b, 115, 30, 30);
	g.fillRect(125, a, 30, 30);
}

	//main public sub
	public static void main(String[] args)throws InterruptedException {
		Game games = new Game();
		//calls private sub window
		window(games);
		while (y>=50) {
			if(y>=80) {
				games.n();}
			games.m();
			games.repaint();
			Thread.sleep(15);}

		}
//private sub window, creates a window
	private static void window(Game games){
		//creates the window with "This is a window" written at the top.
		//JFrame variable is called frame
		JFrame frame = new JFrame("patterns");
		frame.add(games);
		//sets size of window
		frame.setSize(300, 300);
		//sets window to be visible
		frame.setVisible(true);
		//causes program to close when window is exited
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
}
