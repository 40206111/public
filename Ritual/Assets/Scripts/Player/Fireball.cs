﻿using UnityEngine;
using System.Collections;

public class Fireball : MonoBehaviour
{

    private int projSpeed = 3;
    private int direction;
    private float distanceFromPlayer = 0.8f;
    private bool cast;

    private Rigidbody2D fire;

    [SerializeField]
    private Rigidbody2D player;

    private float timer;

    // Use this for initialization
    void Start()
    {
        fire = GetComponent<Rigidbody2D>();
        cast = false;
        gameObject.SetActive(cast);
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.SetActive(cast);
        if (fire.velocity == new Vector2(0, 0) || (Time.time - timer) >= 3)
        {
            cast = false;
            gameObject.SetActive(cast);
        }
        if (cast)
        {
            fire.velocity = new Vector3(projSpeed * direction, 0);
        }
    }

    public void SummonFire()
    {
        timer = Time.time;
        cast = true;
        gameObject.SetActive(cast);
        if (player.GetComponent<PlayerAnimation>().facingRight)
        {
            GetComponent<Transform>().position = new Vector3(player.position.x + distanceFromPlayer, player.position.y - 0.05f);
            direction = 1;
        }
        else
        {
            GetComponent<Transform>().position = new Vector3(player.position.x - distanceFromPlayer, player.position.y - 0.05f);
            direction = -1;
        }
        fire.velocity = new Vector3(projSpeed * direction, 0);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag.Equals("Enemy"))
        {
            cast = false;
            if (other.tag == "Enemy")
            {
                other.GetComponent<EnemyHealth>().health -= 70;
            }
        }
    }
}
