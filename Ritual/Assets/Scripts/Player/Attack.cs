﻿using UnityEngine;
using System.Collections;

public class Attack : MonoBehaviour {

	private GameObject player;
	
	private int dagger = 30;
	private float noSpam;
	// Use this for initialization
	void Start () {
		noSpam = Time.time;
		player = GameObject.Find ("Player");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerStay2D(Collider2D other)
	{
		if (other.tag.Equals ("Enemy"))
		{
			player.GetComponent<PlayerControls> ().enemyInRange = true;
		}
		if ((Time.time - noSpam) > 0.2f)
		{
			if (player.GetComponent<PlayerControls> ().attack)
			{
				if ((player.GetComponent<Transform>().position.x < other.GetComponent<Transform>().position.x && 
					!other.GetComponent<Enemy1Animation>().facingRight) || 
					(player.GetComponent<Transform>().position.x > other.GetComponent<Transform>().position.x && 
						other.GetComponent<Enemy1Animation>().facingRight)){
					Debug.Log ("back attack");
					other.GetComponent<EnemyHealth>().health -= 3*dagger;
				} else
				{
					Debug.Log ("front attack");
					other.GetComponent<EnemyHealth> ().health -= dagger;
					noSpam = Time.time;
				}
			}
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag.Equals ("Enemy"))
			GameObject.Find("Player").GetComponent<PlayerControls> ().enemyInRange = false;
		// Set to false on enemy death
	}
}
