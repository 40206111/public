﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

    private int _health;
    private int _maxHealth = 100;

    [SerializeField]
    private GameObject gm;


    public int health
    {
        get
        {
            return _health;
        }
        set
        {
            _health = Mathf.Clamp(value, 0, maxHealth);
            ShowUI();
			if (value <= 0)
            {
				Debug.Log ("It's happening again!");
				gameObject.GetComponent<Animator> ().SetBool ("Is dead", true);
                gm.GetComponent<LevelManager>().RespawnPlayer();
            }
        }
    }

    public int maxHealth
    {
        get
        {
            return _maxHealth;
        }
        set
        {
            health += value - _maxHealth;
            _maxHealth = value;
        }
    }

    private void ShowUI()
    {
        GameObject.Find("Health Count").GetComponent<Text>().text = health + "/" + maxHealth;
        GameObject.Find("Health Bar Full").GetComponent<RectTransform>().localScale = 
            new Vector3(((float)health / (float)maxHealth), GameObject.Find("Health Bar Full").GetComponent<RectTransform>().localScale.y);
    }

    void Start()
    {
        health = maxHealth;
    }
}
