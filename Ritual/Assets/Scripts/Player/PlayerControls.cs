﻿using UnityEngine;
using System.Collections;

public class PlayerControls : MonoBehaviour
{





	public GameObject pause;
	public GameObject pauseText;



	// stats //
	[SerializeField]
	private float player_speed = 4;
	[SerializeField]
	private float player_jumpHeight = 6.5f;


	// jump stuff //
	public Transform groundCheck;
	public float groundCheckRadius;
	public LayerMask whatIsGround;
	private bool grounded = false;


	// ladder stuff
	private float player_Vvel;
	public bool onLadder;
	private float gravityStore;
	public bool mounted;

	// player variables //
	private float player_vel;
	private Rigidbody2D player;


	// pause //
	public bool isPaused = false;


	// attack
	public bool enemyInRange = false;
	public bool attack = false;

	private float attackTimer;

	// fire timer
	private float fireTimer;




	// Use this for initialization
	void Start ()
	{
		player = GetComponent<Rigidbody2D> ();
        gravityStore = player.gravityScale;
		attackTimer = Time.time;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (isPaused)
		{
			///       Unpause     //////////////////////
			if (Input.GetKeyDown (KeyCode.Escape))
			{
				pause.SetActive (false);
				pauseText.SetActive (false);
				isPaused = false;
				Time.timeScale = 1.0f;
			}
		}
		else
		{




			///         Pause       ////////////////////
			if (Input.GetKeyDown (KeyCode.Escape))
			{
				pause.SetActive (true);
				pauseText.SetActive (true);
				isPaused = true;
				Time.timeScale = 0.0f;
				Debug.Log ("pause " + isPaused);
			}



			///     Horizontal Movement     ///////////////
			player_vel = player_speed * Input.GetAxisRaw ("Horizontal");
			player.velocity = new Vector3 (player_vel, player.velocity.y);



			///        Jump          ///////////////////////////////////
			grounded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, whatIsGround);
			if (Input.GetKeyDown (KeyCode.Space) && grounded && !mounted)
				player.velocity = new Vector3 (player.velocity.x, player_jumpHeight);




			///        Ladder        //////////////////////////////////
			if (onLadder && Input.GetKey (KeyCode.W))
			{
				mounted = true;
			}
			if (onLadder && mounted)
			{
				player.gravityScale = 0;
				player_Vvel = player_speed * Input.GetAxisRaw ("Vertical");
				player.velocity = new Vector3 (player.velocity.x, player_Vvel);
			} else if (!onLadder)
			{
				mounted = false;
				player.gravityScale = gravityStore;
			}



			///     Attack     //////////////////////////////
			attack = false;
			if (Input.GetKeyDown(KeyCode.K))
			{
				if ((Time.time - attackTimer) > 1 / 3.0)
				{
					attackTimer = Time.time;
				}
				GetComponent<Animator> ().SetBool ("Is attacking", true);
				if (enemyInRange)
					attack = true;
			}
			if ((Time.time - attackTimer) >= 1 / 3.0)
			{
				GetComponent<Animator> ().SetBool ("Is attacking", false);
			}



			// Clone ability
			if (Input.GetKeyDown(KeyCode.L)) {
				GetComponent<CloneAbility> ().activate = true;
			}



			// Fireball ability
			if (Input.GetKeyDown(KeyCode.F) && grounded && !GetComponent<Animator>().GetBool("Is moving") && (Time.time - fireTimer) >=5)
			{
				player.velocity = new Vector3(0, 0);
				GetComponent<PlayerAnimation>().Fireball();
				fireTimer = Time.time;
			}



		}/////// Unpaused bracket
	}

	/*void OnCollisionEnter2D(Collision2D other){
		Debug.Log ("Enter please");
		if (other.gameObject.name.Equals("Door")){
			Debug.Log ("Entered");
			GameObject.Find("GM").GetComponent<NextLevel>().LoadScene(1);
		}
	}*/
}
