﻿using UnityEngine;
using System.Collections;

public class onCloneExpire : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnDestroy()
	{
		GameObject[] enemies = GameObject.FindGameObjectsWithTag ("Enemy");
		foreach (GameObject enemy in enemies)
			enemy.GetComponent<cloneLocked> ().locked = false;
	}
}
