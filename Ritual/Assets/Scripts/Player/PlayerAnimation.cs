﻿using UnityEngine;
using System.Collections;

public class PlayerAnimation : MonoBehaviour {

    private Rigidbody2D player;
    [SerializeField]
    private Animator anim;
    public bool facingRight = true;
    private Transform playerTrans;

    [SerializeField]
    private GameObject fireball;
    private float fireCast = 5 / 6.0f;

    // Use this for initialization
    void Start () {
        player = GetComponent<Rigidbody2D>();
        playerTrans = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        if (player.velocity.x > 0)
        {
            anim.SetBool("Is moving", true);
            if (!facingRight)
            {
                facingRight = true;
                playerTrans.localScale = new Vector3(playerTrans.localScale.x * (-1f), playerTrans.localScale.y * 1f);
            }
        } else if (player.velocity.x < 0)
        {
            anim.SetBool("Is moving", true);
            if (facingRight)
            {
                facingRight = false;
                playerTrans.localScale = new Vector3(playerTrans.localScale.x * (-1f), playerTrans.localScale.y * 1f);
            }
        } else
        {
            anim.SetBool("Is moving", false);
        }
        anim.SetBool("Is grounded", Physics2D.OverlapCircle
            (GetComponent<PlayerControls>().groundCheck.position,
            GetComponent<PlayerControls>().groundCheckRadius,
            GetComponent<PlayerControls>().whatIsGround));
	}

	public void NotAttacking (){
		anim.SetBool ("Is attacking", false);
	}

    public void Fireball()
    {
        StartCoroutine("SummonFire");
    }

    private IEnumerator SummonFire()
    {
        anim.SetBool("Fireball", true);
        yield return new WaitForSeconds(fireCast*7/8.0f);
        fireball.GetComponent<Fireball>().SummonFire();
        yield return new WaitForSeconds(fireCast / 8.0f);
        anim.SetBool("Fireball", false);
    }
}
