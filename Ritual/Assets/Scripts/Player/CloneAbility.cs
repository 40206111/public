﻿using UnityEngine;
using System.Collections;

public class CloneAbility : MonoBehaviour {

	// public bool objSpawn = false;
	private float minDist = 20.0f;
	private GameObject closestEnemy;
	[SerializeField]
	private GameObject clone;
	[SerializeField]
	private GameObject cloneFlipped;
	private Object cloneclone;
	public bool activate = false;
	private bool flip = false;
	private bool valid = false;

	private float timer;

	public void Clone () {
		GameObject[] enemies = GameObject.FindGameObjectsWithTag ("Enemy");
		foreach (GameObject enemy in enemies)
		{
			if (Mathf.Abs (GameObject.Find ("Player").transform.position.y - enemy.transform.position.y) < 1.5f)
			{
				if (Mathf.Abs (GameObject.Find ("Player").transform.position.x - enemy.transform.position.x) < minDist)
				{
					minDist = Mathf.Abs (GameObject.Find ("Player").transform.position.x - enemy.transform.position.x);
					closestEnemy = enemy;
					valid = true;
					activate = false;
				} 
			}
		}
		if (minDist > 5)
			valid = false;
		Debug.Log ("x valid " + valid);
		minDist = 20f;
		if (valid && Time.time - timer >= 7f)
		{
			Vector3 pos;
			closestEnemy.GetComponent<cloneLocked> ().locked = true;
			if (GameObject.Find ("Player").transform.position.x - closestEnemy.transform.position.x < 0)
			{
				pos = new Vector3 (closestEnemy.transform.position.x + 0.42f, closestEnemy.transform.position.y);
				flip = true;
			} else
			{
				pos = new Vector3 (closestEnemy.transform.position.x - 0.42f, closestEnemy.transform.position.y);
				flip = false;
			}
			if (flip)
				cloneclone = Instantiate (cloneFlipped, pos, closestEnemy.transform.rotation);
			else
				cloneclone = Instantiate (clone, pos, closestEnemy.transform.rotation);
			Destroy (cloneclone, 3);

			activate = false;
			valid = false;
			timer = Time.time;
		}
	}


	// Use this for initialization
	void Start () {
		timer = Time.time;
	}

	// Update is called once per frame
	void Update () {
		if (activate) {
			Clone ();
		}
	}















	/*
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 







////////// Paste this in enemy AI

		if (!cloneLocked) {
			/////////////////  Enemy AI goes here
		}
		else
		{
			tutAI.velocity = new Vector3 (0, 0);
			if (GameObject.FindGameObjectWithTag ("abilityClone").transform.position.x - transform.position.x > 0) {
				if (transform.localScale.x < 0)
					transform.localScale = new Vector3 (transform.localScale.x * -1, transform.localScale.y);
			}
			else
			if (GameObject.FindGameObjectWithTag ("abilityClone").transform.position.x - transform.position.x < 0)
			if (transform.localScale.x > 0)
				transform.localScale = new Vector3 (transform.localScale.x * -1, transform.localScale.y);
		}



	//////////////////////////////////////////////////












  ///////////  Paste this to player controls

	if (Input.GetKeyDown(KeyCode.L)) {
		GetComponent<CloneAbility> ().activate = true;
	}




	////////////////////////////
	*/
}