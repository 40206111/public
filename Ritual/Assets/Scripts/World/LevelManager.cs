﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

    private PlayerControls player;
    public float respawnDelay;
    public GameObject currentCheckpoint;

    void Start()
    {
        player = FindObjectOfType<PlayerControls>();
    }

    public void RespawnPlayer()
    {
        StartCoroutine("RespawnPlayerCo");
    }

    public IEnumerator RespawnPlayerCo()
    {
        player.enabled = false;
        Debug.Log("Player Respawn");
        yield return new WaitForSeconds(respawnDelay);
        player.transform.position = currentCheckpoint.transform.position;
		player.GetComponent<PlayerHealth>().health = player.GetComponent<PlayerHealth>().maxHealth;
		player.enabled = true;
		player.GetComponent<Animator> ().SetBool ("Is dead", false);
    }
}
