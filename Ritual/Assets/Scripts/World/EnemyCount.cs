﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class EnemyCount : MonoBehaviour {

    private int _count;
    public int count
    {
        get
        {
            return _count;
        }
        set
        {
            _count = value;
            ShowUI(value);
        }
    }

    private void ShowUI(int x)
    {
        GameObject.Find("Enemy Count").GetComponent<Text>().text = (x + "");
    }

    void Update()
    {
		if(count==0){
        count = GameObject.FindGameObjectsWithTag("Enemy").Length;
		}
    }
}
