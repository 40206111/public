﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class NextLevel : MonoBehaviour {
    //camera
    public Camera c;
	public Vector3[] cam_pos = new Vector3[2];
    private int cam_i;
    public int cam_scale = 6;
    public int Vsmooth = 4;
    public int Hsmooth = 4;
    public int Ssmooth = 2;
    private bool move;
    private bool scale;
    private bool up;
    private bool begin = false;
    private bool done = false;
    public int readDelay = 3;
    private int sceneNo;

    //stop scripts
    public CameraFix camFix;
    public PlayerControls player;

    public float delay;

    public void LoadScene(int scene)
    {
        player.enabled = false;
        camFix.enabled = false;
        cam_i = 0;
        move = true;
        scale = true;
        begin = true;
        sceneNo = scene;
    }

    public IEnumerator SceneTransition()
    {
        yield return new WaitForSeconds(delay);
        cam_i = 1;
        up = true;
        done = true;
    }

    public IEnumerator ReadStory()
    {
        Debug.Log("READ!");
        yield return new WaitForSeconds(readDelay);
        SceneManager.LoadScene(sceneNo);
    }

    // Use this for initialization
    void Start () {
        camFix = FindObjectOfType<CameraFix>();
        player = FindObjectOfType<PlayerControls>();
	}
	
	// Update is called once per frame
	void Update () {

        //move to center
        if (move)
        {
            if (cam_pos[cam_i] != c.transform.position)
            {
				c.transform.position = Vector3.MoveTowards(c.transform.position, cam_pos[cam_i], Time.deltaTime * Hsmooth);
            }
            else
            {
                Debug.Log("move done");
                move = false;
            }
        }

        //scale up
        if (scale)
        {
            if (cam_scale != Mathf.Round(c.orthographicSize))
            {
               c.orthographicSize = Mathf.Lerp(c.orthographicSize, cam_scale, Time.deltaTime*Ssmooth);
                Debug.Log(c.orthographicSize);
            }
            else
            {
                Debug.Log("scale done");
                scale = false;
            }
        }

        if (!move && !scale && begin)
        {
            StartCoroutine("SceneTransition");
        }

        //move up
        if (up)
        {
            if (cam_pos[cam_i] != c.transform.position)
            {
                c.transform.position = Vector3.MoveTowards(c.transform.position, cam_pos[cam_i], Time.deltaTime * Vsmooth);
            }
            else
            {
                Debug.Log("up done");
                up = false;
            }
        }

        if (!up && done)
        {
            StartCoroutine("ReadStory");
        }
    }
}
