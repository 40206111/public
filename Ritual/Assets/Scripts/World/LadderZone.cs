﻿using UnityEngine;
using System.Collections;

public class LadderZone : MonoBehaviour {

    private PlayerControls player;

	// Use this for initialization
	void Start ()
    {
        player = FindObjectOfType<PlayerControls>();
	}
	
	// Update is called once per frame
	void OnTriggerStay2D (Collider2D other)
    {
	   if (other.name == "Player")
        {
            player.onLadder = true;
        }
	}
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.name == "Player")
        {
            player.onLadder = false;
        }
    }
}
