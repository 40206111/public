﻿using UnityEngine;
using System.Collections;

public class CameraFix : MonoBehaviour {

	[SerializeField]
	private Transform target;
	public float damping = 0.5f;
	public float lookAheadFactor = 1;
	public float lookAheadReturnSpeed = 0.5f;
	public float lookAheadMoveTreshhold = 0.1f;

	private float m_OffsetZ;
	private Vector3 m_lastTargetPosition;
	private Vector3 m_currentVelocity;
	private Vector3 m_lookAheadPos;


	// Use this for initialization
	void Start ()
	{
		m_lastTargetPosition = target.position;
		m_OffsetZ = (transform.position - target.position).z;
		transform.parent = null;
	}
	
	// Update is called once per frame
	void Update ()
	{
		float xMoveDelta = (target.position - m_lastTargetPosition).x;

		bool updateLookAheadTarget = Mathf.Abs (xMoveDelta) > lookAheadMoveTreshhold;

		if (updateLookAheadTarget)
			m_lookAheadPos = lookAheadFactor * Vector3.right * Mathf.Sign (xMoveDelta);
		else
			m_lookAheadPos = Vector3.MoveTowards (m_lookAheadPos, Vector3.zero, Time.deltaTime * lookAheadReturnSpeed);

		Vector3 aheadTargetPos = target.position + m_lookAheadPos + Vector3.forward * m_OffsetZ;
		Vector3 newPos = Vector3.SmoothDamp (transform.position, aheadTargetPos, ref m_currentVelocity, damping);

		transform.position = newPos;
		m_lastTargetPosition = target.position;
	}
}
