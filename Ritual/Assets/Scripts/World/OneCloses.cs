﻿using UnityEngine;
using System.Collections;

public class OneCloses : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.name.Equals("Player")){
			GameObject.Find("GM").GetComponent<NextLevel>().LoadScene(1);
		}
	}

}
