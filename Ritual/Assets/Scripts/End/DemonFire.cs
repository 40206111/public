﻿using UnityEngine;
using System.Collections;

public class DemonFire : MonoBehaviour {

    private Demon_move demon;
    private float startTime;
    private Rigidbody2D fire;
    private float moveSpeed = 10;
    private bool first = true;
    private int dir = -1;
    private PlayerHealth player;
    private bool fireing = true;
    private int waitTime = 1;
    private Vector3 pos;


	// Use this for initialization
	void Start () {
        demon = FindObjectOfType<Demon_move>();
        fire = GetComponent<Rigidbody2D>();
        player = FindObjectOfType<PlayerHealth>();
    }
	
	// Update is called once per frame
	void Update () {

        if (demon.GetComponent<Demon_move>().right)
        {
            dir = 1;
        } else
        {
            dir = -1;
        }

        if (demon.GetComponent<Demon_move>().attack)
        {
            if (first)
            {
            startTime = Time.time;
                first = false;
            }
            if (Time.time - startTime > waitTime)
            {
                fire.velocity = new Vector3(moveSpeed * dir, fire.velocity.y);
            }
        }
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag.Equals("Player"))
        {
            fire.velocity = new Vector3(0, fire.velocity.y);
            startTime = Time.time;
            waitTime = 2;
            player.GetComponent<PlayerHealth>().health -= 30;
            fire.transform.position = demon.GetComponent<Demon_move>().firePos;
        }
        if (other.name.Equals("Background"))
        {
            fire.velocity = new Vector3(0, fire.velocity.y);
            waitTime = 1;
            startTime = Time.time;
            fire.transform.position = demon.GetComponent<Demon_move>().firePos;
        }
    }
}
