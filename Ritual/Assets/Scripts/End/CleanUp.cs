﻿using UnityEngine;
using System.Collections;

public class CleanUp : MonoBehaviour {

    private float currentTime;

	// Use this for initialization
	void Start () {
        currentTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
	    if(Time.time - currentTime > 5)
        {
            Destroy(gameObject);
        }
	}
}
