﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Demon_move : MonoBehaviour {

    private Rigidbody2D body;
    private bool move = true;
    private Vector3 pos;
    private int smooth = 1;
    public int smooth2 = 4;
    public bool go;
    public Bg_Move bg;
    public int readDelay;
    public bool attack;
    public GameObject[] enemy;
    private Vector3 ePos;
    private bool enemyActive;
    public bool right = false;
    private bool first = true;
    private int asmooth = 10;

    private EnemyHealth health;
    private LevelManager gm;

    private DemonFire fire;
    private bool change = true;
    public Vector3 firePos;



	// Use this for initialization
	void Start () {
        body = GetComponent<Rigidbody2D>();
        bg = FindObjectOfType<Bg_Move>();
        fire = FindObjectOfType<DemonFire>();
        health = GetComponent<EnemyHealth>();
        gm = FindObjectOfType<LevelManager>();
        health.maxHealth = 1;
        pos = new Vector3(body.transform.position.x, body.transform.position.y - 10, body.transform.position.z);
        Debug.Log(health.maxHealth);
	}
	
	// Update is called once per frame
	void Update () {
	    if (move)
        {
            if (pos != body.transform.position)
            {
                body.transform.position = Vector3.MoveTowards(body.transform.position, pos, Time.deltaTime * smooth);
            }
            else
            {
                Debug.Log("move done");
                move = false;
                StartCoroutine("Read");
                pos = new Vector3(body.transform.position.x - 5, body.transform.position.y+ 3);
            }
        }
        if (bg.GetComponent<Bg_Move>().done)
        {
            if (pos != body.transform.position)
            {
                body.transform.position = Vector3.MoveTowards(body.transform.position, pos, Time.deltaTime * smooth2);
            }
            else
            {
                Debug.Log("move done");
                bg.GetComponent<Bg_Move>().done = false;
                attack = true;
            }
        }
        if(attack)
        {
            if (first)
            {
                first = false;
                gm.GetComponent<LevelManager>().RespawnPlayer();
                pos = new Vector3(body.transform.position.x + 5, body.transform.position.y - 7.5f);
            }
            if (change)
            {
                if (pos != body.transform.position)
                {
                    body.transform.position = Vector3.MoveTowards(body.transform.position, pos, Time.deltaTime * asmooth);
                }
                else
                {
                    change = false;
                    firePos = fire.transform.position;
                }
            }

        }

	}

    public IEnumerator Read()
    {
        yield return new WaitForSeconds(readDelay);
        go = true;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag.Equals("Player"))
        {
            SceneManager.LoadScene(4);
        }
    }
}
