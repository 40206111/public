﻿using UnityEngine;
using System.Collections;

public class Bg_Move : MonoBehaviour {

    private Rigidbody2D bg;
    public Demon_move demon;
    private Vector3 pos;
    public int smooth = 100;
    public bool done;

	// Use this for initialization
	void Start () {
        bg = GetComponent<Rigidbody2D>();
        demon = FindObjectOfType<Demon_move>();
        pos = new Vector3(bg.transform.position.x, bg.transform.position.y + 95);
	}
	
	// Update is called once per frame
	void Update () {
	if(demon.GetComponent<Demon_move>().go)
        {
            if (pos != bg.transform.position)
            {
                bg.transform.position = Vector3.MoveTowards(bg.transform.position, pos, Time.deltaTime * smooth);
            }
            else
            {
                Debug.Log("move done");
                demon.GetComponent<Demon_move>().go = false;
                done = true;
            }
        }
	}
}
