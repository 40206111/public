﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour {

    private int _health;
    private int _maxHealth = 100;

    public int health
    {
        get
        {
            return _health;
        }
        set
        {
            if (_health > 0 && value <= 0)
            {
                GameObject.Find("GM").GetComponent<EnemyCount>().count--;
                gameObject.GetComponent<Animator>().SetBool("Is dead", true);
            }
            _health = Mathf.Clamp(value, 0, maxHealth);
        }
    }

    public int maxHealth
    {
        get
        {
            return _maxHealth;
        }
        set
        {
            health += value - _maxHealth;
            _maxHealth = value;
        }
    }

    void Start()
    {
        health = maxHealth;
    }
}
