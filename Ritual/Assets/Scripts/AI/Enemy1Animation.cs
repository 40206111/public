﻿using UnityEngine;
using System.Collections;

public class Enemy1Animation : MonoBehaviour {

	private Rigidbody2D enemy;
	[SerializeField]
	private Animator anim;
	public bool facingRight = true;
	private Transform enemyTrans;
	private float spinMe;

	// Use this for initialization
	void Start () {
		enemy = GetComponent<Rigidbody2D>();
		enemyTrans = GetComponent<Transform>();
		spinMe = Time.time;
	}

	// Update is called once per frame
	void Update () {
		if ((Time.time - spinMe) >= 0.1)
		{
			if (enemy.velocity.x > 0)
			{
				anim.SetBool ("Is moving", true);
				if (facingRight)
				{
					facingRight = false;
					enemyTrans.localScale = new Vector3 (enemyTrans.localScale.x * (-1f), enemyTrans.localScale.y * 1f);
				}
			} else if (enemy.velocity.x < 0)
			{
				anim.SetBool ("Is moving", true);
				if (!facingRight)
				{
					facingRight = true;
					enemyTrans.localScale = new Vector3 (enemyTrans.localScale.x * (-1f), enemyTrans.localScale.y * 1f);
				}
			} else
			{
				anim.SetBool ("Is moving", false);
			}
			spinMe = Time.time;
		}
	}

	public void HurtHim (){
		anim.SetBool ("Is attacking", true);
	}

	public void DontHurtHim(){
		anim.SetBool ("Is attacking", false);
	}
}
