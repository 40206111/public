﻿using UnityEngine;
using System.Collections;

public class Enemy_three_AI : MonoBehaviour
{


    // stats
    [SerializeField]
    private float moveSpeed = 1.2f;
    private float attackDelay = 1;
    private float disengage;

    [SerializeField]
    private bool dummy = false;

    public bool fence = false;
    public int patrolDir = -1;


    [SerializeField]
    public GameObject leftFence;
    [SerializeField]
    private GameObject rightFence;
    [SerializeField]
    private GameObject player;
    public GameObject target;
    private Vector3 targetRange;
    private float dropAgro;
    private float spinMe;

    private Rigidbody2D tutAI;

    public bool cloneLocked = false;




    // Use this for initialization
    void Start()
    {
        tutAI = GetComponent<Rigidbody2D>();
        player = GameObject.Find("Player");
        target = player;
        dropAgro = Time.time;
        spinMe = Time.time;
        disengage = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (!GetComponent<Animator>().GetBool("Is dead"))
        {
            if (!cloneLocked)
            {

                //player detection
                targetRange = player.transform.position - gameObject.transform.position;
                if (targetRange.magnitude < 3f)
                {
                    if ((Time.time - dropAgro) >= 2)
                    {
                        target = player;
                    }
                }

                //direction of travel
                if ((Time.time - spinMe) >= 0.2)
                {
                    if (target.GetComponent<Transform>().position.x < gameObject.GetComponent<Transform>().position.x)
                    {
                        patrolDir = -1;
                    }
                    else if (target.GetComponent<Transform>().position.x > gameObject.GetComponent<Transform>().position.x)
                    {
                        patrolDir = 1;
                    }
                    spinMe = Time.time;
                }

                //stab and go
                /*if (targetRange.magnitude < 0.6f && (Time.time - disengage) >= attackDelay && target.name.Equals (player.name))
                {
                    GetComponent<Enemy1Animation> ().HurtHim ();
                    disengage = Time.time;
                } else if ((Time.time - disengage) >= 1 / 3.0f)
                {*/
                tutAI.velocity = new Vector3(moveSpeed * patrolDir, tutAI.velocity.y);
                //GetComponent<Enemy1Animation> ().DontHurtHim ();
                //}
            }
            else
            {
                tutAI.velocity = new Vector3(0, 0);
                if (GameObject.FindGameObjectWithTag("abilityClone").transform.position.x - transform.position.x > 0)
                {
                    if (transform.localScale.x < 0)
                    {
                        transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y);
                        GetComponent<Enemy1Animation>().facingRight = (!GetComponent<Enemy1Animation>().facingRight);
                    }
                }
                else if (GameObject.FindGameObjectWithTag("abilityClone").transform.position.x - transform.position.x < 0)
                {

                    if (transform.localScale.x > 0)
                    {
                        transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y);
                        GetComponent<Enemy1Animation>().facingRight = (!GetComponent<Enemy1Animation>().facingRight);
                    }
                }
            }
        }
    }

    //Target reassignment
    void OnTriggerStay2D(Collider2D other)
    {
        if ((target.name.Equals(rightFence.name) && other.name.Equals(rightFence.name)))
        {
            target = leftFence;
        }
        else if (target.name.Equals(leftFence.name) && other.name.Equals(leftFence.name))
        {
            target = rightFence;
        }
    }
}