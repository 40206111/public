﻿using UnityEngine;
using System.Collections;

public class Fraud_enemy_health : MonoBehaviour {

	private int _health;
	private int _maxHealth = 100;

	public int health
	{
		get
		{
			return _health;
		}
		set
		{
			Debug.Log ("Damage!");
			_health = Mathf.Clamp(value, 0, maxHealth);
			if (health == 0)
			{
				gameObject.GetComponent<Animator> ().SetBool ("Is dead", true);
			}
		}
	}

	public int maxHealth
	{
		get
		{
			return _maxHealth;
		}
		set
		{
			int x = value;
			_maxHealth = x;
			health += x;
		}
	}

	void Start()
	{
		health = maxHealth;
	}
}
