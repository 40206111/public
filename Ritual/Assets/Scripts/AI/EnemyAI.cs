﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour
{





	public bool playerInRange = false;
	public bool attacking = false;








	// stats
	[SerializeField]
	private float moveSpeed = 1.2f;
	private float attackDelay = 1;
	private float disengage;

	public bool fence = false;
	public int patrolDir = -1;

	[SerializeField]
	public GameObject leftFence;
	[SerializeField]
	private GameObject rightFence;
	[SerializeField]
	private GameObject player;
	public GameObject target;
	private Vector3 targetRange;
	private float dropAgro;
	private float spinMe;

	private Rigidbody2D tutAI;

	public bool cloneLocked = false;


	// Use this for initialization
	void Start ()
	{
		tutAI = GetComponent<Rigidbody2D> ();
		player = GameObject.Find ("Player");
		target = player;
		dropAgro = Time.time;
		spinMe = Time.time;
		disengage = Time.time;
	}

	// Update is called once per frame
	void Update ()
	{
		if (!GetComponent<cloneLocked> ().locked)
		{
			if (!GetComponent<Animator> ().GetBool ("Is dead"))
			{
				
				//player detection
				targetRange = player.transform.position - gameObject.transform.position;
				if (targetRange.magnitude < 3f)
				{
					if ((Time.time - dropAgro) >= 2)
					{
						target = player;
					}
				}

				//direction of travel
				if ((Time.time - spinMe) >= 0.2)
				{
					if (target.GetComponent<Transform> ().position.x < gameObject.GetComponent<Transform> ().position.x)
					{
						patrolDir = -1;
					} else if (target.GetComponent<Transform> ().position.x > gameObject.GetComponent<Transform> ().position.x)
					{
						patrolDir = 1;
					}
					spinMe = Time.time;
				}
			
				//stab and go
				if (playerInRange && (Time.time - disengage) >= attackDelay && target.name.Equals (player.name))
				{
					GetComponent<Enemy1Animation> ().HurtHim ();



					attacking = true;




					disengage = Time.time;
				} else if ((Time.time - disengage) >= 1 / 3.0f)
				{
					if (targetRange.magnitude > 0.6f)
					{
						tutAI.velocity = new Vector3 (moveSpeed * patrolDir, tutAI.velocity.y);
					}
					GetComponent<Enemy1Animation> ().DontHurtHim ();
				}
			}
			// while dead stuff
			else
			{
				tutAI.gravityScale = 0;
				GetComponent<BoxCollider2D> ().isTrigger = true;
				GetComponent<CircleCollider2D> ().isTrigger = true;
				tutAI.velocity = new Vector3 (0, 0);
			}
		} else
		{
			tutAI.velocity = new Vector3 (0, 0);
			if (GameObject.FindGameObjectWithTag ("abilityClone").transform.position.x - transform.position.x > 0)
			{
				if (transform.localScale.x < 0)
				{
					transform.localScale = new Vector3 (transform.localScale.x * -1, transform.localScale.y);
					GetComponent<Enemy1Animation> ().facingRight = (!GetComponent<Enemy1Animation> ().facingRight);
				}
			} else if (GameObject.FindGameObjectWithTag ("abilityClone").transform.position.x - transform.position.x < 0)
			{

				if (transform.localScale.x > 0)
				{
					transform.localScale = new Vector3 (transform.localScale.x * -1, transform.localScale.y);
					GetComponent<Enemy1Animation> ().facingRight = (!GetComponent<Enemy1Animation> ().facingRight);
				}
			}
		}
	}

	//Target reassignment
	void OnTriggerStay2D (Collider2D other)
	{
		if (other.name.Equals (leftFence.name) || other.name.Equals (rightFence.name))
		{
			if (target.name.Equals (player.name))
			{
				if (other.gameObject.name.Equals (leftFence.name))
				{
					target = rightFence;
				} else if (other.gameObject.name.Equals (rightFence.name))
				{
					target = leftFence;
				}
				dropAgro = Time.time;
			} else if (target.name.Equals (rightFence.name) && other.name.Equals (rightFence.name))
			{
				target = leftFence;
			} else if (target.name.Equals (leftFence.name) && other.name.Equals (leftFence.name))
			{
				target = rightFence;
			}
		}
	}
}