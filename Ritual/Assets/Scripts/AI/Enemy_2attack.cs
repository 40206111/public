﻿using UnityEngine;
using System.Collections;

public class Enemy_2attack : MonoBehaviour {

	[SerializeField]
	private GameObject enemy;

	private int dmg = 15;
	private float noSpam;
	private float timer = 0;
	private bool done = false;


	// Use this for initialization
	void Start () {
		noSpam = Time.time;
		//	enemy = GetComponentInParent<BoxCollider2D> ().gameObject;
		Debug.Log (enemy.name);
	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerStay2D(Collider2D other)
	{
		if (other.name.Equals ("Player"))
		{
			enemy.GetComponent<Fraud_enemy_AI> ().playerInRange = true;
		}
		if (enemy.GetComponent<Fraud_enemy_AI> ().attacking && enemy.GetComponent<EnemyHealth>().health > 0
			&& enemy.GetComponent<Fraud_enemy_AI> ().playerInRange)
		{
			timer += Time.deltaTime;
			if (timer > 0.3f && !done)
			{
				other.GetComponent<PlayerHealth> ().health -= dmg;
				done = true;
			}
			if (timer > 4f)
			{
				timer = 0;
				done = false;
			}
		}

	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag.Equals ("Enemy"))
			GameObject.Find("Player").GetComponent<PlayerControls> ().enemyInRange = false;
		// Set to false on enemy death









		if (other.name.Equals ("Player"))
			enemy.GetComponent<EnemyAI> ().playerInRange = false;
	}
}
