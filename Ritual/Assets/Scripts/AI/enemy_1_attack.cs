﻿using UnityEngine;
using System.Collections;

public class enemy_1_attack : MonoBehaviour {


	[SerializeField]
	private GameObject enemy;

	private int dmg = 8;
	private float attackCooldown = 0.3f;
	private float noSpam;
	private float timer = 0;
	private bool done = false;


	// Use this for initialization
	void Start () {
		noSpam = Time.time;
	//	enemy = GetComponentInParent<BoxCollider2D> ().gameObject;
		Debug.Log (enemy.name);
	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerStay2D(Collider2D other)
	{
		if (other.name.Equals ("Player"))
		{
			enemy.GetComponent<EnemyAI> ().playerInRange = true;
		}
		if (enemy.GetComponent<EnemyAI> ().attacking == true)
		{
			timer += Time.deltaTime;
			if (timer > 0.1f && !done)
			{
				other.GetComponent<PlayerHealth> ().health -= dmg;
				done = true;
			}
			if (timer > 1f)
			{
				timer = 0;
				done = false;
			}
		}

	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag.Equals ("Enemy"))
			GameObject.Find("Player").GetComponent<PlayerControls> ().enemyInRange = false;
		// Set to false on enemy death









		if (other.name.Equals ("Player"))
			enemy.GetComponent<EnemyAI> ().playerInRange = false;
	}
}
