﻿using UnityEngine;
using System.Collections;

public class Enemy3_AI : MonoBehaviour {

    private Rigidbody2D hammerMan;

    private bool inRange;
    private bool followRange;
    private bool canAttack = true;
    private bool attacking;
    private bool hit;
    private bool first = true;
    public float radius;
    public float followRadius;
    public float hitRadius;
    public Transform attackZone;
    public Transform hitZone;
    public LayerMask whoIsPlayer;
    public LayerMask whereIsHit;
    public GameObject leftFence;
    private bool hitem;

    private int i = 0;

   // private Collider2D collider;

    private Enemy_three_AI enemy;
    private PlayerControls player;

    [SerializeField]
    private Animator anim;

    // Use this for initialization
    void Start () {
        hammerMan = GetComponent<Rigidbody2D>();
        enemy = FindObjectOfType<Enemy_three_AI>();
        player = FindObjectOfType<PlayerControls>();
        GetComponent<EnemyHealth>().maxHealth = 400;
    }
	
	// Update is called once per frame
	void Update () {
        if (!anim.GetBool("Is dead"))
        {
            inRange = Physics2D.OverlapCircle(attackZone.position, radius, whoIsPlayer);
            followRange = Physics2D.OverlapCircle(attackZone.position, followRadius, whoIsPlayer);

            if (followRange)
            {
                enemy.target = player.gameObject;
            }
            if (!followRange && enemy.target == player.gameObject)
            {
                enemy.target = leftFence;
            }
            if (inRange)
            {
                Attack();

            }
            if (attacking)
            {
                hit = Physics2D.OverlapCircle(hitZone.position, hitRadius, whereIsHit);
                if (hit)
                {
                    while (!hitem)
                    {
                        Debug.Log("Hit");
                        player.GetComponent<PlayerHealth>().health -= 45;
                        hitem = true;
                    }
                }
            }
        }
        else
        {
            GetComponent<Collider2D>().isTrigger = true;
            GetComponent<CircleCollider2D>().isTrigger = true;
            hammerMan.gravityScale = 0;
            hammerMan.velocity = new Vector3(0, 0);
        }
    }

    public void Attack()
    {
        StartCoroutine("attackPlayer");
    }

    public IEnumerator attackPlayer()
    {
        if (canAttack)
        {
            canAttack = false;
            attacking = true;
            anim.SetBool("Is attacking", true);
        }
        else {
            yield return new WaitForSeconds(2);
            hitem = false;
            attacking = false;
            anim.SetBool("Is attacking", attacking);
            canAttack = true;
            first = true;
        }
        
    }
}
