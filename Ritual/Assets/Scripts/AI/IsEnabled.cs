﻿using UnityEngine;
using System.Collections;

public class IsEnabled : MonoBehaviour {

	public bool enabled = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!enabled)
		{
			gameObject.SetActive (false);
		}
	}
}
