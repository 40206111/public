﻿using UnityEngine;
using System.Collections;

public class Fraud_spawner : MonoBehaviour {


	public GameObject enemy1;
	public GameObject enemy2;
	public GameObject enemy3;
	public GameObject enemy4;
	public GameObject enemy5;
	public GameObject enemy6;
	public GameObject enemy7;
	public GameObject enemy8;

	public GameObject fake1;
	public GameObject fake2;
	public GameObject fake3;
	public GameObject fake4;
	public GameObject fake5;
	public GameObject fake6;
	public GameObject fake7;
	public GameObject fake8;

	GameObject [] enemies = new GameObject[8];
	GameObject[] fakes = new GameObject[8];
	public int alive = 4;
	int safety = 0;
	int fAlive = 4;

	private float fakeTimer = 4;

	// Use this for initialization
	void Start ()
	{
		enemies [0] = enemy1;
		enemies [1] = enemy2;
		enemies [2] = enemy3;
		enemies [3] = enemy4;
		enemies [4] = enemy5;
		enemies [5] = enemy6;
		enemies [6] = enemy7;
		enemies [7] = enemy8;

		fakes [0] = fake1;
		fakes [1] = fake2;
		fakes [2] = fake3;
		fakes [3] = fake4;
		fakes [4] = fake5;
		fakes [5] = fake6;
		fakes [6] = fake7;
		fakes [7] = fake8;
		for (int i = 0; i < 4; i++)
		{
			int r = Random.Range (0, 8);
			if (enemies [r].activeInHierarchy)
				i--;
			else
			{
				enemies [r].SetActive (true);
			}
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		fAlive = 0;
		foreach (GameObject f in fakes)
			if (f.activeInHierarchy)
				fAlive++;
		alive = 0;
		foreach (GameObject e in enemies)
			if (e.activeInHierarchy)
				alive++;
		if (fakeTimer > 4)
		{
			// fake spawning
			for (int i = 0; i < alive; i++)
			{
				int r = Random.Range (0, 8);
				if (enemies [r].activeInHierarchy)
					i--;
				else
				{
					if (!fakes [r].activeInHierarchy)
					{
						fakes [r].GetComponent<IsEnabled> ().enabled = true;
						fakes [r].SetActive (true);
						Debug.Log ("spawn");
					}
					else
						i--;
				}
				safety++;
				if (safety > 5000)
				{
					i = 4;
				//	alive = 0;
					Debug.Log("safety");
				}
			}
			fakeTimer = 0;
		} else
			if (alive > fAlive)
				fakeTimer += Time.deltaTime;
	}	
}
