﻿using UnityEngine;
using System.Collections;

//3.4   -120.72

public class Intro : MonoBehaviour {

    public Animator anim;
    private Rigidbody2D player;
    private bool scale = false;
    private int cam_scale = 10;
    private Vector3 cam_pos;
    public Camera c;
    public int Ssmooth = 1;
    private bool first = true;
    private bool move;
    public int Hsmooth = 1;

    public Transform groundCheck;
    public float groundCheckRadius;
    public LayerMask whatIsGround;
    public bool grounded = false;


    // Use this for initialization
    void Start () {
        anim.SetBool("Is grounded", true);
        player = GetComponent<Rigidbody2D>();
        player.gravityScale = 0;
    }

    // Update is called once per frame
    void Update () {

        grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);

        if (Time.time > 1 && Time.time < 1.5)
        {
            anim.SetBool("Is dead", true);
        }
        if (Time.time > 2 && Time.time < 2.5)
        {
            player.gravityScale = 1;
            anim.SetBool("Is dead", false);
            anim.SetBool("Is grounded", false);
            GetComponent<Collider2D>().isTrigger = true;
            scale = true;
        }
        if(scale)
        {
            if (cam_scale != Mathf.Round(c.orthographicSize))
            {
                c.orthographicSize = Mathf.Lerp(c.orthographicSize, cam_scale, Time.deltaTime * Ssmooth);
                Debug.Log(c.orthographicSize);
            }
            else
            {
                    Debug.Log("scale done");
                    scale = false;
            }
        }
        if (move)
        {
            if (cam_pos != c.transform.position)
            {
                c.transform.position = Vector3.MoveTowards(c.transform.position, cam_pos, Time.deltaTime * Hsmooth);
            }
            else
            {
                Debug.Log("move done");
                move = false;
            }
        }
        if (grounded)
        {
            cam_scale = 4;
            scale = true;
            anim.SetBool("Is grounded", true);
            move = true;
            cam_pos = new Vector3(c.transform.position.x, -127, c.transform.position.z);

        }

	}
}
