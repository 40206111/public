﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Demon_Intro : MonoBehaviour {

    private Rigidbody2D body;
    private bool move = true;
    private Vector3 pos;
    public int smooth = 1;
    public Intro intro;
    private bool first;
    public int readDelay;

    // Use this for initialization
    void Start () {
        body = GetComponent<Rigidbody2D>();
        pos = new Vector3(3.4f, -125.72f, body.transform.position.z);
        intro = FindObjectOfType<Intro>();
        move = true;
    }
	
	// Update is called once per frame
	void Update () {
	    if (intro.GetComponent<Intro>().grounded)
        {
            if (move)
            {
                if (pos != body.transform.position)
                {
                    body.transform.position = Vector3.MoveTowards(body.transform.position, pos, Time.deltaTime * smooth);
                }
                else
                {
                    Debug.Log("move done");
                    move = false;
                    StartCoroutine("Read");
                }
            }
        }
    }

    public IEnumerator Read()
    {
        yield return new WaitForSeconds(readDelay);
        SceneManager.LoadScene(0);
    }
}
