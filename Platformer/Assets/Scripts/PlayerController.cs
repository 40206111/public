﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public float moveSpeed;
    public float moveVelocity;
    public float jumpHeight;

    public Transform groundCheck;
    public float groundCheckRadius;
    public LayerMask whatIsGround;
    private bool grounded;

    private bool doubleJumped;

    private Animator anim;

    private Rigidbody2D rigidPlayer;

    public bool onLadder;
    public float climbSpeed;
    private float climbVelocity;
    private float gravityStore;

    void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
    }

	// Use this for initialization
	void Start ()
    {
        anim = GetComponent<Animator>();
        rigidPlayer = GetComponent<Rigidbody2D>();
        gravityStore = rigidPlayer.gravityScale;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (grounded)
        {
            doubleJumped = false;
        }

        anim.SetBool("Grounded", grounded);
        anim.SetBool("OnLadder", onLadder);

        if (Input.GetKeyDown (KeyCode.Space) && grounded)
        {
            jump();
        }
        if (Input.GetKeyDown(KeyCode.Space) && !doubleJumped && !grounded)
        {
            jump();
            doubleJumped = true;
        }

        moveVelocity = moveSpeed * Input.GetAxisRaw("Horizontal");
        rigidPlayer.velocity = new Vector2(moveVelocity,rigidPlayer.velocity.y);

        if (rigidPlayer.velocity.x < 0)
        {
            rigidPlayer.transform.rotation = Quaternion.Euler(0, 180, 0);
        }else
        {
            rigidPlayer.transform.rotation = Quaternion.Euler(0, 0, 0);
        }

        anim.SetFloat("Speed", Mathf.Abs(rigidPlayer.velocity.x));
        anim.SetFloat("VSpeed", Mathf.Abs(rigidPlayer.velocity.y));

        if(onLadder)
        {
            rigidPlayer.gravityScale = 0f;
            climbVelocity = climbSpeed * Input.GetAxisRaw("Vertical");
            rigidPlayer.velocity = new Vector2(rigidPlayer.velocity.x, climbVelocity);
        }else if(!onLadder)
        {
            rigidPlayer.gravityScale = gravityStore;
        }
    }

    public void jump()
    {
        rigidPlayer.velocity = new Vector2(rigidPlayer.velocity.x, jumpHeight);
    }
}
