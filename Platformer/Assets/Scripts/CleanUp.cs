﻿using UnityEngine;
using System.Collections;

public class CleanUp : MonoBehaviour {

    private ParticleSystem thisPSystem;

	// Use this for initialization
	void Start ()
    {
        thisPSystem = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if (thisPSystem.isPlaying)
        {
            return;
        }
        Destroy(gameObject);
    }
}
